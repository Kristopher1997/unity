using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Incluse the namespace required for Unity UI
using TMPro;

public class PlayerController : MonoBehaviour
{
    // Create public variables for the Text UI game objects
    public TextMeshProUGUI countText;
    public GameObject winTextObject;

    private int count;

    // At the start of the game..
    void Start()
    {
        // Set the count to zero
        count = 0;

        SetCountText();

        // Set the text property of the Win Text UI to an empty string, making the 'You Win' (game over message) blanck
        winTextObject.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        // ..and if the GameObject you interect has the tag 'PickUp' assigned to it...
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);  

            // Add one to the score variable 'count'
            count = count + 1;

            // Run the 'SetCountText()' function
            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();

        if(count >= 14)
        {
            // Set the text value of your 'winText'
            winTextObject.SetActive(true);
        }
    }
}
